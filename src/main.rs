#![feature(plugin)]
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
//#[macro_use] extern crate serde_json;
#[macro_use] extern crate serde_derive;

use rocket_contrib::json::{Json, JsonValue};

mod hero;
use hero::{Hero};

#[get("/")]
fn hero_list() -> JsonValue {
    json!([
        "hero 1", 
        "hero 2"
    ])
}

#[post("/", data = "<hero>")]
fn hero_create(hero: Json<Hero>) -> Json<Hero> {
    hero
}

#[put("/<_id>", data = "<hero>")]
fn hero_update(_id: i32, hero: Json<Hero>) -> Json<Hero> {
    hero
}

#[delete("/<_id>")]
fn hero_delete(_id: i32) -> Json<JsonValue> {
    Json(json!({"status": "ok"}))
}

#[get("/<name>/<age>")]
fn hello(name: String, age: u8) -> String {
  format!("Hello, {} year old named {}!", age, name)
}


fn main() {
  rocket::ignite()
    .mount("/api/hello", routes![hello])
    .mount("/api/hero", routes![hero_create, hero_delete, hero_update])
    .mount("/api/heros", routes![hero_list])
    .launch();
}