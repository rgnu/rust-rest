extern crate rust_rest;
extern crate diesel;

use self::rust_rest::models::*;
use self::diesel::prelude::*;

fn main() {
    use self::rust_rest::schema::posts::dsl::*;
    use self::rust_rest::{establish_connection};

    let connection = establish_connection();
    let results = posts
        //.filter(published.eq(false))
        .order(id.asc())
        .limit(5)
        .load::<Post>(&connection)
        .expect("Error loading posts");

    println!("Displaying {} posts", results.len());
    for post in results {
        println!(
            "[#{}][Publish:{}] {}\n----------\n{}", 
            post.id, post.published, post.title,  post.body
        );
    }
}