extern crate rust_rest;
extern crate diesel;

use std::env::args;

use self::diesel::prelude::*;
use self::rust_rest::*;
use self::models::Post;

fn main() {
    use self::rust_rest::schema::posts::dsl::{posts, published};
    use self::rust_rest::{establish_connection};

    let id = args().nth(1).expect("publish_post requires a post id")
        .parse::<i32>().expect("Invalid ID");

    let connection = establish_connection();

    let post = diesel::update(posts.find(id))
        .set(published.eq(true))
        .get_result::<Post>(&connection)
        .expect(&format!("Unable to find post {}", id));
    println!("Published post {}", post.title);
}