all: build

build: OUT_DIR?=$(CURDIR)
build: FLAGS?=--release --all
build:
	cargo build $(FLAGS)

release: FLAGS:=--release
release: build

run:
	cargo run